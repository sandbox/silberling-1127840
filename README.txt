********************************************************************
            P A G E    T I T L E    F I E L D  M O D U L E
********************************************************************
Original Author: Robert Silbiger

********************************************************************
DESCRIPTION:

   This module enables a field formatter for any text field so it
   can be used as a page title in the browser.

********************************************************************
INSTALLATION:

1. Place the entire page_title_field directory into your Drupal modules/
   directory or the sites modules directory (eg site/default/modules)


2. Enable this module by navigating to:

     Administer > Build > Modules

3. Configure the entity you want to control the page title for.
   For nodes, go to admin/structure/types;
   for taxonomy terms, go to admin/structure/taxonomy;

   - Add a new field of type "Text"  - optionally set a default value
     For example, I generally use "[node:title] | [site:name]"
   - Set its display type to "Use as Page Title"
   - Make sure the field is enabled for the display type, generally disable
     the label display - no field value will be displayed so the label may
     look out of place if enabled.
     For example, for taxonomy terms, I use the "Taxonomy term page" display type.

4. The page title is ultimately set at the theme level. To let your PHPTemplate
   based theme interact with this module, you need to add some code to the template.php
   file that comes with your theme. If there is no template.php file, you can simply
   use the one included with this download. Here is the code:

function _phptemplate_variables($hook, $vars) {
  $vars = array();
  if ($hook == 'page') {

    if (module_exists('page_title_field')) {
      $vars['head_title'] = page_title_field_get_title($vars['head_title']);
    }

  }
  return $vars;
}

OR - which is what I would prefer

  Edit html.tpl.php in your theme template folder -

  Replace
    <title><?php print $head_title; ?></title>

  With
    <title><?php
      if (module_exists('page_title_field')) {
        print page_title_field_get_title($head_title);
      }
      else {
        print $head_title;
      }
    ?></title>
